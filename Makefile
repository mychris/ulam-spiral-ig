# SPDX-License-Identifier: ISC
# SPDX-FileCopyrightText: © 2022 Christoph Göttschkes
# SPDX-FileType: SOURCE
# See the LICENSE file in the root directory of this repository

.SUFFIXES:
.SUFFIXES: .o .c

CC ?= gcc -pipe
#DBG = -O0 -gdwarf-4
DBG =

COMPILE.c = ${CC} ${CFLAGS} ${DBG} ${CPPFLAGS} -c
LINK.c = ${CC} ${CFLAGS} ${DBG} ${CPPFLAGS} ${LDFLAGS}

.IMPSRC = $<
.TARGET = $@

CPPFLAGS += -D_POSIX_C_SOURCE=200809L
CFLAGS += -std=c11 -O2
CFLAGS += -Wall -Wextra -pedantic -Werror \
	-Wshadow -Wpointer-arith -Wcast-qual -Wconversion\
	-Wstrict-prototypes -Wmissing-prototypes \
	-Wfloat-equal -Wsign-compare -Wformat=2 \
	-Wunused-function -Wundef -Wunused-value -Wreturn-type \
	-Wuninitialized -Wno-parentheses -Wno-empty-body \
	-Wno-implicit-fallthrough -Wno-shift-negative-value
CFLAGS += -fno-omit-frame-pointer -fstack-protector-strong \
	-fno-common -fstrict-aliasing

SRC != find . -name '*.c'
OBJ := ${SRC:.c=.o}

all: ulam-spiral-ig

ulam-spiral-ig: ${OBJ}
	${LINK.c} -o ${.TARGET} ${OBJ} ${LDLIBS}

.c.o:
	${COMPILE.c} ${.IMPSRC}

fmt:
	astyle --style=knf --indent=tab --break-return-type --max-code-length=80 ${SRC}

clean:
	find . -name '*.o' -delete
	${RM} ulam-spiral-ig

SRC: ; @echo ${SRC}
OBJ: ; @echo ${OBJ}
CFLAGS: ; @echo ${CFLAGS}
CPPFLAGS: ; @echo ${CPPFLAGS}
LDFLAGS: ; @echo ${LDFLAGS}
CC: ; @echo ${CC}

.PHONY: all fmt clean SRC OBJ CFLAGS CPPFLAGS LDFLAGS CC

/* SPDX-License-Identifier: ISC */
/* SPDX-FileCopyrightText: © 2022 Christoph Göttschkes */
/* SPDX-FileType: SOURCE */
/* See the LICENSE file in the root directory of this repository */

#include <sys/time.h>

#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <inttypes.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

typedef uint_fast8_t bitset_arr_type;
struct bitset {
	size_t len;
	size_t allocated;
	bitset_arr_type *bits;
};
#define BITSET_POS_INDEX(p) ((p) / (sizeof(bitset_arr_type) * CHAR_BIT))
#define BITSET_POS_BIT(p) ((p) & (sizeof(bitset_arr_type) * CHAR_BIT - 1))

static void
bitset_set(struct bitset *bs, size_t position)
{
	bs->bits[BITSET_POS_INDEX(position)] |=
	    ((bitset_arr_type) 1) << BITSET_POS_BIT(position);
}

static void
bitset_set_all(struct bitset *bs)
{
	memset(bs->bits, 0xFF, bs->allocated);
}

static void
bitset_unset(struct bitset *bs, size_t position)
{
	bs->bits[BITSET_POS_INDEX(position)] &=
	    (bitset_arr_type) ~(((bitset_arr_type) 1) << BITSET_POS_BIT(position));
}

static void
bitset_unset_all(struct bitset *bs)
{
	memset(bs->bits, 0x00, bs->allocated);
}

static int
bitset_get(struct bitset *bs, size_t position)
{
	return 0 != (bs->bits[BITSET_POS_INDEX(position)]
	             & (((bitset_arr_type) 1) << BITSET_POS_BIT(position)));
}

static size_t
bitset_len(struct bitset *bs)
{
	return bs->len;
}

static int
bitset_init(struct bitset *bs, size_t number_of_bits)
{
	bs->len = number_of_bits;
	bs->allocated =  (number_of_bits / sizeof(bitset_arr_type)) + sizeof(
	                     bitset_arr_type);
	bs->bits = (bitset_arr_type*) malloc(bs->allocated);
	if (!bs->bits) {
		bs->len = 0;
		bs->allocated = 0;
		return -1;
	}
	/* this should just be bitset_unset_all(b) */
	/* since not all functions might be used here */
	/* otherwise the compiler might issue a warning */
	bitset_set_all(bs);
	bitset_unset_all(bs);
	bitset_set(bs, 0);
	bitset_unset(bs, 0);
	(void) bitset_get(bs, 0);
	(void) bitset_len(bs);
	return 0;
}

static void
bitset_free(struct bitset *bs)
{
	if (bs->bits) {
		free(bs->bits);
		bs->bits = NULL;
		bs->allocated = 0;
		bs->bits = 0;
	}
}

struct iteration_state {
	size_t width;
	size_t height;
	unsigned char *data;
	size_t prime_lookup_length;
	struct bitset prime_lookup;
};

static void
iteration_state_mark_pixel(struct iteration_state *is, size_t x, size_t y)
{
	is->data[x + y * is->width] = 1;
}

static int
iteration_state_is_marked(struct iteration_state *is, size_t x, size_t y)
{
	return is->data[x + y * is->width];
}

static int
iteration_state_is_prime(struct iteration_state *is, uint64_t number)
{
	if (number <= is->prime_lookup_length)
		return bitset_get(&is->prime_lookup, number);
	return -1;
}

static size_t
max_size_t(const size_t a, const size_t b)
{
	return (a > b) ? a : b;
}

static size_t
isqrt_size_t(size_t x)
{
	size_t m, y, b;
	m = ((size_t) 1u) << ((sizeof(size_t) * CHAR_BIT) - 2u);
	y = 0;
	while (m != 0) {
		b = y | m;
		y = y >> 1;
		if (x >= b) {
			x = x - b;
			y = y | m;
		}
		m = m >> 2;
	}
	return y;
}

static int
prime_sieve(struct bitset *nums, const size_t limit)
{
	if (-1 == bitset_init(nums, limit + 1))
		return -1;
	bitset_set_all(nums);
	bitset_unset(nums, 0);
	if (limit >= 1)
		bitset_unset(nums, 1);
	for (size_t i = 2; i <= isqrt_size_t(limit + 1) + 1; ++i) {
		if (bitset_get(nums, i)) {
			for (size_t j = i * i; j <= limit; j += i)
				bitset_unset(nums, j);
		}
	}
	return 0;
}

typedef void (*iterate_number_spiral_cb)(void*, uint64_t, uint64_t, uint64_t);

/*
static void
iterate_number_spiral_printer(void* state, uint64_t num, uint64_t x, uint64_t y)
{
	(void) state;
	printf("%" PRIu64 " %" PRIu64 " %" PRIu64 "\n", num, x, y);
}
*/

static void
iterate_number_spiral_fill_img(void *state, uint64_t num, uint64_t x,
                               uint64_t y)
{
	struct iteration_state *is = (struct iteration_state*) state;
	int is_prime = iteration_state_is_prime(is, num);
	if (is_prime == -1)
		abort();
	if (is_prime)
		iteration_state_mark_pixel(is, x, y);
}

static void
iterate_number_spiral(size_t width, size_t height,
                      iterate_number_spiral_cb callback, void *state)
{
	const uint64_t start = 1;
	const uint64_t end = (width > height) ? width * width : height * height;
	uint64_t edge_len = 1;
	uint64_t edge_even_odd = 1;
	int64_t direction_x = 1, direction_y = 0;
	uint64_t x = width / 2, y = height / 2;
	uint64_t num = start;

	callback(state, start, x, y);
	num += 1;
	while (num <= end) {
		for (uint64_t i = 0; i < edge_len && num <= end; ++i) {
			x = (direction_x < 0)
			    ? x - (uint64_t)(-direction_x)
			    : x + (uint64_t)direction_x;
			y = (direction_y < 0)
			    ? y - (uint64_t)(-direction_y)
			    : y + (uint64_t)direction_y;
			if (x < width && y < height)
				callback(state, num, x, y);
			num += 1;
		}
		if (edge_even_odd == 0) {
			edge_even_odd = 1;
			edge_len += 1;
		} else
			edge_even_odd = 1 - edge_even_odd;

		if (direction_x != 0) {
			direction_y = -direction_x;
			direction_x = 0;
		} else {
			direction_x = direction_y;
			direction_y = 0;
		}
	}
}

static int
save_to_image(struct iteration_state *is, const char *filename,
              uint32_t foreground, uint32_t background, size_t scale_factor)
{
	if (!filename || *filename == '\0') {
		errno = EINVAL;
		return -1;
	}
	char fg[16] = {0};
	char bg[16] = {0};
	char ppm_type[8] = {0};
	char colors[8] = {0};

	if ((foreground == 0x000000 || foreground == 0x00FFFFFF)
	    && (background == 0x00000000 || background == 0x00FFFFFF)) {
		strcpy(ppm_type, "P1");
		strcpy(colors, "");
		strcpy(fg, (foreground == 0x000000) ? "0\n" : "1\n");
		strcpy(bg, (background == 0x000000) ? "0\n" : "1\n");
	} else {
		unsigned int fg_r = (foreground >> 16) & 0xFF;
		unsigned int fg_g = (foreground >> 8) & 0xFF;
		unsigned int fg_b = (foreground >> 0) & 0xFF;
		unsigned int bg_r = (background >> 16) & 0xFF;
		unsigned int bg_g = (background >> 8) & 0xFF;
		unsigned int bg_b = (background >> 0) & 0xFF;

		if (fg_r == fg_g && fg_g == fg_b && bg_r == bg_g && bg_g == bg_b) {
			strcpy(ppm_type, "P2");
			strcpy(colors, "255");
			snprintf(fg, 16, "%u\n", fg_r);
			snprintf(bg, 16, "%u\n", bg_r);
		} else {
			strcpy(ppm_type, "P3");
			strcpy(colors, "255");
			snprintf(fg, 16, "%u %u %u\n", fg_r, fg_g, fg_b);
			snprintf(bg, 16, "%u %u %u\n", bg_r, bg_g, bg_b);
		}
	}

	FILE *fp;
	if (filename[0] == '-' && filename[1] == '\0')
		fp = stdout;
	else
		fp = fopen(filename, "wb");
	if (!fp)
		return -1;
	fprintf(fp, "%s\n# Ulam spiral\n%zu %zu %s\n", ppm_type,
	        is->width * scale_factor,
	        is->height * scale_factor, colors);
	for (size_t y = 0; y < is->height; ++y) {
		for (size_t s1 = 0; s1 < scale_factor; ++s1) {
			for (size_t x = 0; x < is->width; ++x) {
				char *color = iteration_state_is_marked(is, x, y) ? fg : bg;
				for (size_t s2 = 0; s2 < scale_factor; ++s2)
					fputs(color, fp);
			}
		}
	}
	fflush(fp);
	if (fp != stdout)
		fclose(fp);
	return 0;
}

static int
parse_color(const char *color_string, uint32_t *color)
{
	char *endptr = NULL;
	long long parse_result = 0;
	if (!color_string) {
		errno = EINVAL;
		return -1;
	}
	if (0 != strncmp("0x", color_string, 2)) {
		errno = EINVAL;
		return -1;
	}
	parse_result = strtoll(color_string, &endptr, 16);
	if ((parse_result == LLONG_MIN || parse_result == LLONG_MAX) && errno == ERANGE)
		return -1;
	if (*endptr != '\0') {
		errno = EINVAL;
		return -1;
	}
	if (parse_result < 0 || parse_result > 0x00FFFFFFLL) {
		errno = ERANGE;
		return -1;
	}
	*color = (uint32_t) parse_result;
	return 0;
}

static int
parse_size(const char *size_string, size_t *size)
{
	char *endptr = NULL;
	long long parse_result = 0;
	if (!size_string) {
		errno = EINVAL;
		return -1;
	}
	if (*size_string == '-' || *size_string == '+') {
		errno = EINVAL;
		return -1;
	}
	parse_result = strtoll(size_string, &endptr, 10);
	if ((parse_result == LLONG_MIN || parse_result == LLONG_MAX) && errno == ERANGE)
		return -1;
	if (*endptr != '\0') {
		errno = EINVAL;
		return -1;
	}
	if (parse_result < 0 || ((long long) (size_t) parse_result) != parse_result
	    || (size_t) parse_result > SIZE_MAX) {
		errno = ERANGE;
		return -1;
	}
	*size = (size_t) parse_result;
	return 0;
}

int
main(int argc, char *argv[])
{
	uint32_t foreground_color = 0x00FFFFFF;
	uint32_t background_color = 0x00000000;
	char *out_file = "-";
	size_t width = 200;
	size_t height = 200;
	size_t scale_factor = 1;
	int c;
	while ((c = getopt(argc, argv, "b:f:o:s:")) != -1) {
		switch (c) {
		case 'b':
			if (parse_color(optarg, &background_color) == -1)
				err(1, "%s: Not a valid color", optarg);
			break;
		case 'f':
			if (parse_color(optarg, &foreground_color) == -1)
				err(1, "%s: Not a valid color", optarg);
			break;
		case 'o':
			out_file = optarg;
			break;
		case 's':
			if (parse_size(optarg, &scale_factor) == -1)
				err(1, "%s: Not a valid scale factor", optarg);
			break;
		}
	}
	argc -= optind;
	argv += optind;
	if (argc > 0 && -1 == parse_size(*argv, &width))
		err(1, "%s: Not a valid size", *argv);
	argc--;
	argv++;
	if (argc > 0 && -1 == parse_size(*argv, &height))
		err(1, "%s: Not a valid size", *argv);
	argc--;
	argv++;
#define SIMPLE_BENCH 0
#if SIMPLE_BENCH == 1
	struct timespec start, stop;
#endif

	size_t max_prime = max_size_t(width * width, height * height);
#if SIMPLE_BENCH == 1
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
#endif
	struct bitset primes = {0};
	if (-1 == prime_sieve(&primes, max_prime))
		err(1, "Allocate prime sieve");
#if SIMPLE_BENCH == 1
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
	fprintf(stderr, "prime_sieve:           %20lluns\n",
	        1000000000 * (unsigned long long)(stop.tv_sec - start.tv_sec) +
	        (unsigned long long)(stop.tv_nsec - start.tv_nsec));
#endif
	unsigned char *spiral_data = malloc(sizeof(unsigned char) * max_size_t(
	                                        width * width, height * height));
	if (spiral_data == NULL)
		err(1, "Allocate spiral data");
	memset(spiral_data, 0, sizeof(unsigned char) * max_size_t(width * width,
	        height * height));
	struct iteration_state is = {
		.width = width,
		.height = height,
		.data = spiral_data,
		.prime_lookup_length = max_prime,
		.prime_lookup = primes,
	};
#if SIMPLE_BENCH == 1
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
#endif
	iterate_number_spiral(width, height, iterate_number_spiral_fill_img, &is);
#if SIMPLE_BENCH == 1
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
	fprintf(stderr, "iterate_number_spiral: %20lluns\n",
	        1000000000 * (unsigned long long)(stop.tv_sec - start.tv_sec) +
	        (unsigned long long)(stop.tv_nsec - start.tv_nsec));
#endif
#if SIMPLE_BENCH == 1
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &start);
#endif
	save_to_image(&is, out_file, foreground_color, background_color, scale_factor);
#if SIMPLE_BENCH == 1
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &stop);
	fprintf(stderr, "save_to_image:         %20lluns\n",
	        1000000000 * (unsigned long long)(stop.tv_sec - start.tv_sec) +
	        (unsigned long long)(stop.tv_nsec - start.tv_nsec));
#endif
	bitset_free(&is.prime_lookup);
	return 0;
}
